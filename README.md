# README #

## Getting Started


### Installing


* ``npm install``

* ``NODE_ENV='development' NODE_CONFIG_DIR=./config  pm2 start --name 'node-user-login' app.js --log-date-format "YYYY-MM-DD HH:mm Z"``

* ``NODE_ENV='production' NODE_CONFIG_DIR=./config  pm2 start --name 'node-user-login' app.js --log-date-format "YYYY-MM-DD HH:mm Z"``

* Open ``http://localhost:8027/`` in your browser to get started.

