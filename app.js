/**
 * @module app.js
 * Server Creation and APIs routes
 */

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
config = require('config');

// Connect to mysql database
connection = undefined;
const mysqlLib = require("./lib/mysqlLib");
const userSignup = require("./routes/userSignup");

const app = express();

app.set("port", config.get("port"));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, "views")));

app.get("/getSomething", (req, res) => res.send("Ok"));

app.post("/userSignUp", userSignup.userSignUp);

const httpServer = http.createServer(app).listen(app.get("port"), function () {
    console.log("Express server listening on port " + app.get("port"));
});
