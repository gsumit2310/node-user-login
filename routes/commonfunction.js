/**
 * @module commonfunction
 * This contains common functions to be used in project
 */
const bcrypt = require('bcrypt');


exports.executePromiseConnectionQuery = executePromiseConnectionQuery;
exports.executeHandlerQueryPromise = executeHandlerQueryPromise;
exports.validateEmail = validateEmail;
exports.cryptPassword = cryptPassword;
exports.comparePassword = comparePassword;


var executePromiseConnectionQuery = function (connection_name, query, values) {

    return new Promise((resolve, reject) => {

        const task = connection_name.query(query, values, function (err, data) {

            console.log(task.sql);

            if (err) {
                console.log([err, task.sql]);
                return reject([err, task.sql]);
            }
            return resolve(data);
        });
    });
};

var executeHandlerQueryPromise = function (connection_name, query, values) {

    return new Promise((resolve, reject) => {

        const task = handler.getInstance().executeQuery(query, values, function (err, data) {

            console.log(task.sql);

            if (err) {
                console.log([err, task.sql]);
                return reject([err, task.sql]);
            }
            return resolve(data);
        });
    });
};


function validateEmail(email) {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
}


var cryptPassword = function (password) {

    return new Promise((resolve, reject) => {

        bcrypt.genSalt(10, function (err, salt) {
            if (err)
                return reject(err);

            bcrypt.hash(password, salt, function (err, hash) {
                return resolve(hash);
            });
        });
    });
};

var comparePassword = function (plainPass, hashword) {

    return new Promise((resolve, reject) => {

        bcrypt.compare(plainPass, hashword, function (err, isPasswordMatch) {
            if (err)
                return reject(err);

            return resolve(isPasswordMatch);
        });
    });
};