/**
 * @module userLogin
 * This handles user sign up
 */

const async = require("async");
const moment = require('moment');
const co = require('co');

const writeOnlyHandler = require('../lib/databaseHandler').writeOnlyHandler;
const utils = require('../routes/commonfunction');

exports.userSignUp = userSignUp;

function userSignUp(req, res) {

    const coWrap = co.wrap(function *gen() {

        const params = req.body;
        const userObj = {};

        params.hashed_password = yield utils.cryptPassword(params.password);

        yield userMicroService(params, userObj);
        yield  subscriptionMicroService(params, userObj);

        const response = {
            log: 'Operation successful',
            flag: 143
        };

        return res.send(response);

    });

    coWrap()
        .catch(err => {
            console.log(err);
            return res.status(500).send(err)
        });
}


function userMicroService(params, userObj) {

    return new Promise((resolve, reject) => {

        const userParams = {
            user_name: params.user_name,
            user_email: params.user_email,
            password: params.hashed_password
        };

        if (!utils.validateEmail(userParams.user_email)) {
            reject({
                "Error": "Error in userMicroService",
                "Msg": "Email is invalid"
            });
        }

        const insertUserQuery = `Insert into tb_users Set ?`;

        utils.executeHandlerQueryPromise(writeOnlyHandler, insertUserQuery, userParams)
            .then(data => {
                // as user_id will be primary key of tb_users table
                userObj['user_id'] = data.insertId;

                return resolve();
            })
            .catch(err => reject(err));
    })
}

function subscriptionMicroService(params, userObj) {

    const valid_upto = moment().add('years', 1).format('YYYY-MM-DD');

    const subscriptionParams = {
        user_id: userObj.user_id,
        subscription_type: params.subscription_type,
        valid_upto: valid_upto
    };

    const subscriptionAllowedValues = [];

    for (let key in subscriptionsModels) {
        subscriptionAllowedValues.push(key)
    }

    // else we can store this in db too as enum data type
    // for now validation in code
    if (subscriptionAllowedValues.indexOf(subscriptionParams.subscription_type) == -1) {
        return new Promise.reject({
            "Error": "Error in subscriptionMicroService",
            "Msg": "This subscription service is not allowed"
        });
    }

    // sub_id is primary key tb_users_subscription table
    const insertSubscriptionQuery = `Insert into tb_users_subscription Set ?`;
    return utils.executeHandlerQueryPromise(writeOnlyHandler, insertSubscriptionQuery, subscriptionParams);
}

// later will move to constants
var subscriptionsModels = {
    1: "1 Visit",
    5: "5 Visit",
    22: "22 Visit"
};

//********** SCHEMA ****************//
// tb_users
/**
 CREATE TABLE `tb_users` (
 `user_id` int(11) NOT NULL AUTO_INCREMENT,
 `user_name` varchar(100) NOT NULL,
 `user_email` varchar(100) NOT NULL,
 `password` varchar(100) NOT NULL,
 `access_token` varchar(100) NOT NULL,
 `phone_no` varchar(100) NOT NULL,
 `referral_code` varchar(100) NOT NULL,
 `device_type` tinyint(1) NOT NULL,
 `device_name` varchar(100) NOT NULL,
 `date_registered` timestamp NOT NULL,
 `city` int(2) NOT NULL,
 `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`user_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 **/

// tb_users_subscription
/**
 CREATE TABLE `tb_users_subscription` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `subscription_type` int(11) NOT NULL,
 `valid_upto` timestamp NOT NULL,
 `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 **/