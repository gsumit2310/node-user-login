const mysql = require('mysql');
const config = require('config');

const db_config = {
    host: config.get('DBConfig.writeOnly.host'),
    user: config.get('DBConfig.writeOnly.user'),
    password: config.get('DBConfig.writeOnly.password'),
    database: config.get('DBConfig.writeOnly.dbName')
};


function handleDisconnect() {
    connection = mysql.createConnection(db_config); // Recreate the connection, since
                                                    // the old one cannot be reused.

    connection.connect(function (err) {                 // The server is either down
        if (err) {                                      // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err, db_config);
            setTimeout(handleDisconnect, 2000);         // We introduce a delay before attempting to reconnect,
        }                                               // to avoid a hot loop, and to allow our node script to
    });                                                 // process asynchronous requests in the meantime.

    // If you're also serving http, display a 503 error.
    connection.on('error', function (err) {
        console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {  // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } else {                                        // connection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });

}

handleDisconnect();