const mysql = require('mysql');
const config = require('config');

const db_config = {
    host: config.get('DBConfig.writeOnly.host'),
    user: config.get('DBConfig.writeOnly.user'),
    password: config.get('DBConfig.writeOnly.password'),
    database: config.get('DBConfig.writeOnly.dbName'),
    connectionLimit: config.get('DBConfig.writeOnly.connectionLimit')
};


let numConnectionsInPool = 0;
const numEnqueueOccurred = 0;

function initializePool(dbPoolConfig) {
    console.log('CALLING INITIALIZE POOL');
    const dbConnectionsPool = mysql.createPool(dbPoolConfig);

    dbConnectionsPool.on('connection', function (connection) {
        numConnectionsInPool++;
        console.log('NUMBER OF CONNECTION IN POOL : ' + numConnectionsInPool);
    });
    return dbConnectionsPool;
}

const writeOnlyConnectionsPool = initializePool(db_config);

const writeOnlyClient = {
    executeQuery: function (sql, values, callback) {

        let queryObject = {};

        if (typeof sql === 'object') {
            // query(options, cb)
            queryObject = sql;
            if (typeof values === 'function') {
                queryObject.callback = values;
            } else if (typeof values !== 'undefined') {
                queryObject.values = values;
            }
        } else if (typeof values === 'function') {
            // query(sql, cb)
            queryObject.sql = sql;
            queryObject.values = undefined;
            queryObject.callback = values;
        } else {
            // query(sql, values, cb)
            queryObject.sql = sql;
            queryObject.values = values;
            queryObject.callback = callback;
        }

        return writeOnlyConnectionsPool.query(queryObject.sql, queryObject.values, function (err, result) {
            if (err) {
                if (err.code === 'ER_LOCK_DEADLOCK' || err.code === 'ER_QUERY_INTERRUPTED') {
                    setTimeout(module.exports.writeOnlyHandler.getInstance().executeQuery.bind(null, sql, values, callback), 50);
                }
                else {
                    queryObject.callback(err, result);
                }
            }
            else {
                queryObject.callback(err, result);
            }
        });
    },

    executeTransaction: function (queries, values, callback) {
    },

    escape: function (values) {
        return writeOnlyConnectionsPool.escape(values);
    }
};


exports.writeOnlyHandler = (function () {
    let handler = null;
    return {
        getInstance: function () {
            if (!handler) {
                handler = writeOnlyClient;
            }
            return handler;
        }
    };
})();
